package com.shop.demo.dto;

import lombok.*;

import javax.persistence.Column;

/**
 * Price
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private String productId;

    private String productName;

    private Integer price;

    private Boolean isDiscounted;

    private Integer quantity;

    private Integer discountedPrice;

}