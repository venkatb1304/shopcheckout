package com.shop.demo.dto;

import io.swagger.models.auth.In;
import lombok.*;

/**
 * Price
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Price {

    private Integer price;

}