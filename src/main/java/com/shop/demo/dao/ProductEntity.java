package com.shop.demo.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "product")
public class ProductEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", length = 50, updatable = false, nullable = false)
    private String id;

    @Column(name = "product_id", nullable = false, columnDefinition = "varchar(5)")
    private String productId;

    @Column(name = "product_name", nullable = false, columnDefinition = "varchar(30)")
    private String productName;

    @Column(name = "price", nullable = false)
    private Integer price;

    @Column(name = "is_discounted", nullable = false)
    private Boolean isDiscounted;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "discounted_price")
    private Integer discountedPrice;

}