package com.shop.demo.configuration;

import com.shop.demo.api.StoreApi;
import com.shop.demo.dto.Price;
import com.shop.demo.dto.Product;
import com.shop.demo.service.StoreService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@ConfigurationProperties(prefix = "store")
@Component
@Getter
@Setter
public class StoreConfig {

    private List<Product> products;

}