package com.shop.demo.components;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationEventListener implements ApplicationListener {

    @Override
    public void onApplicationEvent(final ApplicationEvent event) {
        if(event instanceof ApplicationStartingEvent){
            log.info("Application is Starting");
        }
    }
}
