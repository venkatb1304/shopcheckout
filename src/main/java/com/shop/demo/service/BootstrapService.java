package com.shop.demo.service;

import com.shop.demo.configuration.StoreConfig;
import com.shop.demo.dao.ProductEntity;
import com.shop.demo.repository.StoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
@Slf4j
public class BootstrapService {
    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private StoreConfig storeConfig;

    @EventListener(ApplicationReadyEvent.class)
    public void saveProducts() {
        log.info("Saving Products to DB");
        storeConfig.getProducts().stream().forEach(product -> {
            storeRepository.save(
                    ProductEntity.builder()
                            .productId(product.getProductId())
                            .productName(product.getProductName())
                            .price(product.getPrice())
                            .isDiscounted(product.getIsDiscounted())
                            .quantity(product.getQuantity())
                            .discountedPrice(product.getDiscountedPrice())
                            .build());
        });
    }

}
