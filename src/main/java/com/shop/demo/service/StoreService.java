package com.shop.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.shop.demo.dao.ProductEntity;
import com.shop.demo.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shop.demo.dto.Price;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StoreService {

    @Autowired
    private StoreRepository storeRepository;

    public Price checkout(List<String> items) {
        HashMap<String, Integer> productMap = new HashMap<String, Integer>();
        for (String item : items) {
            if (productMap.containsKey(item)) {
                Integer productCount = productMap.get(item);
                productMap.put(item, productCount+1);
            } else {
                productMap.put(item, 1);
            }

        }
        Price price = getFinalPrice(productMap);
        return price;
    }

    public Price getFinalPrice(HashMap<String, Integer> productMap) {
        Integer finalCost = 0;
        for (Map.Entry<String, Integer> entry : productMap.entrySet()) {
            String productId = entry.getKey();
            Integer quantity = entry.getValue();
            log.info("productId::{}", productId);
            log.info("quantity::{}", quantity);
            Optional<List<ProductEntity>> optionalProducts = storeRepository.findByProductId(productId);
            Integer productCost = 0;
            if (optionalProducts.isPresent()) {
                ProductEntity productEntity = optionalProducts.get().get(0);
                if (productEntity.getIsDiscounted()) {
                    if (quantity < productEntity.getQuantity()) {
                        productCost = productEntity.getPrice() * quantity;
                    } else {
                        if (quantity % productEntity.getQuantity() == 0) {
                            productCost = (quantity / productEntity.getQuantity()) * productEntity.getDiscountedPrice();
                        } else {
                            productCost = (quantity / productEntity.getQuantity()) * productEntity.getDiscountedPrice();
                            Integer remainingProducts = quantity % productEntity.getQuantity();
                            productCost = productCost + (productEntity.getPrice() * remainingProducts);
                        }
                    }
                } else {
                    productCost = productEntity.getPrice() * quantity;
                }
                finalCost = finalCost + productCost;
            }
        }
        log.info("Final Cost::{}", finalCost);
        return Price.builder()
                .price(finalCost)
                .build();
    }
}