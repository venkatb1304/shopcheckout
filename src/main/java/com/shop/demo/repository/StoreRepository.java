package com.shop.demo.repository;

import java.util.List;
import java.util.Optional;

import com.shop.demo.dao.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface StoreRepository extends JpaRepository<ProductEntity, String> {

    Optional<List<ProductEntity>> findByProductId(@Param("productId") String productId);
}