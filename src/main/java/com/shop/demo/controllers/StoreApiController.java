package com.shop.demo.controllers;

import com.shop.demo.api.StoreApi;
import com.shop.demo.dto.Price;
import com.shop.demo.service.StoreService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class StoreApiController implements StoreApi {

    @Autowired
    StoreService service;

    public Price placeOrder(List<String> items) {
        return service.checkout(items);
    }

}