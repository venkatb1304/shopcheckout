package com.shop.demo.service;

import com.shop.demo.dao.ProductEntity;
import com.shop.demo.dto.Price;
import com.shop.demo.repository.StoreRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class StoreServiceTest {

    @Mock
    private StoreRepository storeRepository;

    @InjectMocks
    private StoreService storeService;

    @Test
    public void testGetPrice() {
        List<String> productList = new ArrayList<>();
        productList.add("101");
        List<ProductEntity> productEntityList = testData_getProductEntityList();
        storeRepository.save(testData_getProductEntity());
        when(storeRepository.findByProductId(any())).thenReturn(Optional.of(productEntityList));

        Price price = storeService.checkout(productList);
        assertEquals("100", price.getPrice().toString());


    }


    private List<ProductEntity> testData_getProductEntityList() {
        List<ProductEntity> productEntityList = new ArrayList<>();
        ProductEntity productEntity = ProductEntity.builder()
                .id("1")
                .productId("101")
                .productName("Rolex")
                .price(100)
                .isDiscounted(true)
                .quantity(3)
                .discountedPrice(200)
                .build();
        productEntityList.add(productEntity);
        return productEntityList;
    }

    private ProductEntity testData_getProductEntity() {
        return ProductEntity.builder()
                .id("1")
                .productId("101")
                .productName("Rolex")
                .price(100)
                .isDiscounted(true)
                .quantity(3)
                .discountedPrice(200)
                .build();
    }

}
