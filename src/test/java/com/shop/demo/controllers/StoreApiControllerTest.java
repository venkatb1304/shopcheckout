package com.shop.demo.controllers;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shop.demo.dto.Price;
import com.shop.demo.service.StoreService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StoreApiControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    private StoreService storeService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void givenProductList() throws Exception {
        List<String> productList = new ArrayList<>();
        productList.add("101");
        Price price = Price.builder()
                .price(100)
                .build();
        when(storeService.checkout(productList)).thenReturn(price);
        MvcResult mvcResult = mvc.perform(post("/checkout")
        .contentType(APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(productList))
        .accept(APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String contentAsString = mvcResult.getResponse().getContentAsString();
        assertTrue(contentAsString != null);
        assertTrue(contentAsString.contains("100"));
    }

}