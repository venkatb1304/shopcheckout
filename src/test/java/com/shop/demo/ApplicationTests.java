package com.shop.demo;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
public class ApplicationTests {

    @Test
    public void contextLoads() {
        assertThat(true, is(true));
    }
}
