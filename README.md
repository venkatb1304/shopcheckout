### Java Spring Rest Project
####Dependencies to Run in local
1. You need a system with ide and jdk 1.8 and above
2. Using h2 in mem db so no db setup required
3. You can change product data by doing modification in application-products.yml

####To test
* [http://localhost:8080/checkout](http://localhost:8080/checkout)
